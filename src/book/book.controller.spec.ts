import { Test, TestingModule } from '@nestjs/testing';
import { BookController } from './book.controller';
import { BookService } from "./book.service";
import { InputBookDto } from "../dto/input-book.dto";
import { BookDto } from "../dto/book.dto";

describe('BookController', () => {
  let controller: BookController;
  let mockBookService: BookService;

  beforeEach(async () => {
    mockBookService = {} as BookService;

    const module: TestingModule = await Test.createTestingModule({
      controllers: [BookController],
      providers: [{
        provide: BookService,
        useValue: mockBookService
      }],
    }).compile();

    controller = module.get<BookController>(BookController);
  });

  it('should return the list of books returned by bookService.getBooks()', () => {
    mockBookService.getBooks = () => [{id: '1',title: 'test',author: 'test', release_date: '2024'}];
    const books = controller.getBooks();
    expect(books).toEqual([{id: '1',title: 'test',author: 'test', release_date: '2024'}]);
  });

  it('should return the single book returned by bookService.getBookById()', () => {
    //undefined hiba (strict: true-val hibád dob)
    mockBookService.getBookById = (id:string)=> {
      if(id === '1') {
        return {id: '1',title: 'test',author: 'test', release_date: '2024'};
      }
    }
    const book = controller.getBookById('1'); //act
    expect(book).toEqual({id: '1',title: 'test',author: 'test', release_date: '2024'}); //assert
  });

  it('should return the created book returned by bookService.createBook()', () => {
    mockBookService.createBook = (input:InputBookDto) => ({id: '1', ...input}); //arrange
    const book = controller.createBook({title: 'test',author: 'test', release_date: '2024'}); //act
    expect(book).toEqual({id: '1',title: 'test',author: 'test', release_date: '2024'}); //assert
  });


  it('should return the updated book returned by bookService.update()', () => {
    mockBookService.updateBook = (id:string,input: InputBookDto) => {
      return {id, ...input};
    }
    const updateBook   = controller.updateBook({title: 'test',author: 'test', release_date: '2024'}, '1');//act
    expect(updateBook).toEqual({id: '1',title: 'test',author: 'test', release_date: '2024'}); //assert
  });

  it('should call bookService.deleteBook()', () => {
    mockBookService.deleteBook = jest.fn(); //arrange
    controller.deleteBook('1');    //act
    expect(mockBookService.deleteBook).toHaveBeenCalledWith('1'); //assert
  });

});
