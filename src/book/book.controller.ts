import { Body, Controller, Delete, Get, Param, Post, Put } from "@nestjs/common";
import { BookService } from "./book.service";
import { BookDto } from "../dto/book.dto";
import { InputBookDto } from "../dto/input-book.dto";

@Controller('book')
export class BookController {

  constructor(private readonly bookService: BookService) {
  }

  @Get('/all')
  getBooks(): BookDto[] {
    return this.bookService.getBooks();
  }

  @Get('/:id')
  getBookById(@Param('id') id: string): BookDto {
    return this.bookService.getBookById(id);
  }

  @Post('/create')
  createBook(@Body() bookToCreate: InputBookDto) {
    return this.bookService.createBook(bookToCreate);
  }

  @Put('/update/:id')
  updateBook(@Body() bookToUpdate: InputBookDto, @Param('id') id: string ) {
    return this.bookService.updateBook(id, bookToUpdate);
  }

  @Delete('/delete/:id')
  deleteBook(@Param('id') id: string ) {
    return this.bookService.deleteBook(id);
  }
}
