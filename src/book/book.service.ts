import { Injectable, NotFoundException } from "@nestjs/common";
import { BookDto } from "../dto/book.dto";
import { InputBookDto } from "../dto/input-book.dto";

let books: BookDto[] = [];

@Injectable()
export class BookService {

    getBooks(): BookDto[] {
      return books;
    }

    getBookById(id: string) {
      const findBook = books.find(book => book.id === id)
      if (!findBook) {
        throw new NotFoundException();
      }
      return findBook;
    }

    createBook(bookToCreate: InputBookDto) {
      const pushingBook: BookDto = {
        id: Math.random().toString(),
        ...bookToCreate,
      }
      books.push(pushingBook);
      return pushingBook;

    }

    updateBook(id: string, updateBook: InputBookDto) {
      const bookToUpdate: BookDto | undefined = books.find(book => book.id === id);
      if (bookToUpdate === undefined) {
        throw new NotFoundException();
      }
      return Object.assign(bookToUpdate,updateBook);
    }

    deleteBook(id: string) {
      const deleteIndex = books.findIndex(book => book.id === id);
      if (deleteIndex !== -1) {
        books.splice(deleteIndex,1);
      } else {
        throw new NotFoundException();
      }
    }

  clearBooks(): void {
    books = [];
  }

}