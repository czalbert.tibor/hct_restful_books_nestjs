import { Test, TestingModule } from '@nestjs/testing';
import { BookService } from './book.service';
import { NotFoundException } from "@nestjs/common";
import { BookDto } from "../dto/book.dto";

describe('BookService', () => {
  let service: BookService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BookService],
    }).compile();

    service = module.get<BookService>(BookService);
    service.clearBooks();
  });

  describe('read', () => {
    it('should be empty by default', () => {
      const books = service.getBooks(); //act
      expect(books).toEqual([]); //assert
    });

    it('should return a single book after create', () => {
      const createdBook: BookDto = service.createBook({title: 'test', author: 'test', release_date: '2024'}); //arrange
      const book: BookDto | undefined = service.getBookById(createdBook.id);  //act
      expect(book).toEqual({ id: expect.any(String),title: 'test', author: 'test', release_date: '2024'}); //assert
    });

    it('should throw a NotFoundException if id is unknown', () => {
      expect(() => {service.getBookById('42')}).toThrow(NotFoundException);
    });
  })

  describe('create', () => {
    it('should return a single book after create', () => {
      service.createBook({title: 'test', author: 'test', release_date: '2024'}) //act

      expect(service.getBooks()).toEqual([{id: expect.any(String),title: 'test', author: 'test', release_date: '2024'}]); //assert
    });
  })

  describe('createTwoBook', () => {
    it('should return two book after create', () => {
      service.createBook({title: 'test', author: 'test', release_date: '2024'}) //act
      service.createBook({title: 'test1', author: 'test1', release_date: '2024'})

      expect(service.getBooks()).toEqual([{id: expect.any(String),title: 'test', author: 'test', release_date: '2024'},{id: expect.any(String),title: 'test1', author: 'test1', release_date: '2024'}]); //assert
    });
  })

  describe('update', () => {
    it('should return the updated book after the update', () => {
      const book: BookDto = service.createBook({title: 'test', author: 'test', release_date: '2024'}); //arrange
      const updatedBook = service.updateBook(book.id,{title: 'updated', author: 'updated', release_date: '2100'}) //act

      expect(updatedBook).toEqual({id: book.id,title: 'updated', author: 'updated', release_date: '2100'}) //assert
    });

    it('should return the updated book with getBookById', () => {
      const book: BookDto = service.createBook({title: 'test', author: 'test', release_date: '2024'}); //arrange
      service.updateBook(book.id,{title: 'update', author: 'update', release_date: '2100'}); //act

      expect(service.getBookById(book.id)).toEqual({id: expect.any(String),title: 'update', author: 'update', release_date: '2100'}) //assert
    });

    it('should throw NotFoundException if the book is not exist', () => {
      expect(() => {
        service.updateBook('321312',{title: 'update', author: 'update', release_date: '2100'});
      }).toThrow(NotFoundException);
    });
  })

  describe('delete', () => {
    it('should return the new book list after deleted an element', () => {
      service.createBook({title: 'first', author: 'first', release_date: '2000'});
      const bookToDelete = service.createBook({title: 'second', author: 'second', release_date: '2050' });
      service.createBook({title: 'third', author: 'third', release_date: '2100'});//arrange

      service.deleteBook(bookToDelete.id);

      expect(service.getBooks()).toEqual([
        {id: expect.any(String),title: 'first', author: 'first', release_date: '2000'},
        {id: expect.any(String),title: 'third', author: 'third', release_date: '2100'}
      ]); //assert
    });

    it('should return undefined id if the id param is not found', () => {
      service.createBook({title: 'first', author: 'first', release_date: '2000'});
      service.createBook({title: 'second', author: 'second', release_date: '2050'  });//arrange

      expect(() => {
        service.deleteBook('124124'); //act
      }).toThrow(NotFoundException);
    });

  });
});
