export interface InputBookDto {
  title: string,
  author: string,
  release_date: string
}